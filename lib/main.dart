
import 'dart:async';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:internalization/FadeAnimation.dart';
import 'package:internalization/PageTwo.dart';

void main() {

  runApp(
    EasyLocalization(
      supportedLocales: [Locale('en','US'),Locale('es','US'),Locale('fr','FA')],
      path: 'assets',
      fallbackLocale:Locale('en','US'),
      child: MaterialApp(home: MyApp()),

    ),

  );

}

class MyApp extends StatefulWidget {
  @override
  _State createState() => new _State();
}



//State is information of the application that can change over time or when some actions are taken.
class _State extends State<MyApp>{

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      supportedLocales: context.supportedLocales,
      localizationsDelegates: context.localizationDelegates,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        backgroundColor: Colors.cyan,

        appBar: AppBar(
          title: Text("Yoda's Travel"),
          centerTitle: true,
          backgroundColor: Colors.indigo,
          elevation: 0,
        ),

        body:
        ListView(
        children: <Widget>[
          SizedBox(height:5),
              Center(
                child: Image.asset('assets/collage.jpg',
                  fit: BoxFit.cover,
                  height: 300,
                  width: 400,
                ),
              ),
          SizedBox(height:15),
              Center(
                child:FadeAnimation(1.5, Text('Welcome to Travel Explore',textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.indigo,
                      fontSize: 30, fontWeight: FontWeight.bold),).tr()),
                ),
          SizedBox(height:10),
              Center(
                child: Text('Plan your trip quick and easy with us.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ).tr(),
              ),

          SizedBox(height:15),
              Center(
                child: Text('Select Language.', textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.indigo,
                    fontFamily: 'Pacifico',
                    fontSize: 20,
                  ),
                ).tr(),
              ),
              SizedBox(height:10),
              RaisedButton(
                onPressed: () {
                  context.locale=Locale('en','US');
                },
                child: Text("English",
                  style: TextStyle(
                    color: Colors.white,
                  ),).tr(),
                color: Colors.indigo,
              ),
              SizedBox(height: 15),
              RaisedButton(
                onPressed: () {
                  context.locale=Locale('es','US');
                },
                child: Text("Spanish",
                  style: TextStyle(
                    color: Colors.white,
                  ),).tr(),
                color: Colors.indigo,
              ),
              SizedBox(height: 15),

              RaisedButton(
                onPressed: () {
                  context.locale=Locale('fr','FA');
                },
                child: Text("French",
                style: TextStyle(
                  color: Colors.white,
                ),).tr(),
                color: Colors.indigo,
              ),

          Container(
              margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child:
              RaisedButton(
                onPressed: () =>
                    Navigator.of(context,rootNavigator: true).push(MaterialPageRoute(
                        builder: (context) => new PageTwo())),
                child: Text('Information on safety during covid-19!!').tr(),
                textColor: Colors.indigo,
                color: Colors.white,
                padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
              )
          ),
            ],
        ),

      ),

    );
  }
}

