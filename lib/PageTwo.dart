import 'package:flutter/material.dart';
import 'dart:async';
import 'package:easy_localization/easy_localization.dart';
import 'main.dart';
import 'dataTable.dart';
import 'chip.dart';


class PageTwo extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
        //visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // home: Scaffold(
      // backgroundColor: Colors.cyan,
      //
      // appBar: AppBar(
      // title: Text("Yoda's Travel"),
      // centerTitle: true,
      // backgroundColor: Colors.indigo,
      // elevation: 0,
      // ),
      home: MyHomePage(title: "Yoda's Travel",

      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

enum Answers { YES, NO, MAYBE }

class _MyHomePageState extends State<MyHomePage> {

  String _value = '';

  void _setValue(String value) => setState(() => _value = value);

  Future _askUser() async {
    switch (
    await showDialog(

        context: context, useRootNavigator: true,
        /*it shows a popup with few options which you can select, for option we
        created enums which we can use with switch statement, in this first switch
        will wait for the user to select the option which it can use with switch cases*/
        child: new SimpleDialog(
          title: new Text('Our Services').tr(),
          children: <Widget>[
            new SimpleDialogOption(child: new Text('Train.').tr()
            ),
            new SimpleDialogOption(child: new Text('Bus.').tr()),
            new SimpleDialogOption(child: new Text('Flight.').tr()),
          ],
        )
    )
    ) {
      case Answers.YES:
        _setValue('Train.');
        break;
      case Answers.NO:
        _setValue('Bus.');
        break;
      case Answers.MAYBE:
        _setValue('Flight.');
        break;
    }
  }

  final globalKey = GlobalKey<ScaffoldState>();

  Future<String> createAlertDialog(BuildContext context) {
    TextEditingController customController = TextEditingController();
    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text(
            "Please enter your Name to agree on wearing mask and sanitizing your hands for your travel?")
            .tr(),
        content:
        TextField(
          controller: customController,
        ),
        actions: <Widget>[
          MaterialButton(
            elevation: 5.0,
            child: Text('Submit').tr(),
            onPressed: () {
              Navigator.of(context, rootNavigator: true).pop(
                  customController.text.toString());
            },
          )
        ],
      );
    });
  }


  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      key: globalKey,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body:
      Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.

          child:
          ListView(
              children: <Widget>[
                // for (int i = 0; i < 2; i++)
                Column(

                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 10),
                    Text("Thank you for choosing Yoda's Travel",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.indigo,
                        fontSize: 30,
                      ),).tr(),
                    MyStatelessWidget(),
                    SizedBox(height: 20),

                    // Container(
                    //   child: Image.asset('assets/images/starter-image.jpg',
                    //     fit: BoxFit.cover,
                    //     height: 300,
                    //     width: 400,
                    //   ),
                    // ),
                    Container(
                      // margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child:
                        FlatButton(
                          onPressed: () {
                            createAlertDialog(context).then((onValue) {
                              if (onValue != null && onValue != "") {
                                final snackBar = SnackBar(content: Text(
                                    "Hello $onValue, You have agreed to take necessary safety precautions!")
                                    .tr());
                                globalKey.currentState.showSnackBar(snackBar);
                              }
                            }
                            );
                          },
                          child: Text('Covid-19 Alert Consent').tr(),
                          textColor: Colors.white,
                          color: Colors.red,
                          padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
                        )

                    ),
                    SizedBox(height: 10),
                    Container(
                      child: Center(
                        child: Column(

                          children: <Widget>[
                            // RaisedButton(onPressed: _askUser,
                            //
                            //   child: new Text('Services',
                            //     style: TextStyle(
                            //       color: Colors.white,
                            //     ),).tr(),
                            //   color: Colors.redAccent,)
                            RoundedButtonWithIcon(
                              onPressed: _askUser,
                              icon: Icons.card_travel,
                              title: "Services for travel offered by the company".tr(),
                              buttonColor: Colors.indigo,
                            ),

                          ],
                        ),

                      ),
                    ),
                    SizedBox(height: 10),
                    GredientButton(onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => HomeApp()));
                      //Navigate to chips
                    },
                      splashColor: Colors.orange,
                      colors: [
                        Color(0xFF6200EA),
                        Color(0xFFB388FF),
                      ], title: "Gredient Chips button".tr(),),
                    SizedBox(height: 10),
                    PrimaryLineButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => DataTableInfo()));
                        //Navigator.of(context).push(MaterialPageRoute(
                          //  builder: (context) => DataTableInfo()));
                      },
                      title: "For more information on Data Tables".tr(),
                    ),
                    SizedBox(height: 10),
                    Container(
                      //    margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child:

                      // RaisedButton(
                      //   onPressed: () =>
                      //       Navigator.of(context).push(MaterialPageRoute(
                      //           builder: (context) => new MyApp())),
                      //   child: Text('Back').tr(),
                      //   textColor: Colors.white,
                      //   color: Colors.red,
                      //   padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
                      // )
                      FloatingIconButton(
                        icon: Icons.home,
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => new MyApp()));
                        },
                      ),
                    ),

                  ],

                ),
              ])
      ),
    );
  }
}




class MyStatelessWidget extends StatelessWidget {
  MyStatelessWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5), // if you need this
            side: BorderSide(
              color: Colors.grey.withOpacity(.2),
              width: 1,
            ),
          ),
          child: Container(
            width: 400,
            height: 240,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 13.0, 8.0, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 50.0,
                        width: 60.0,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15.0),
                          child: Image.asset('assets/collage.jpg',
                            fit: BoxFit.cover,
                            height: 300,
                            width: 400,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 6.0, 0, 0),
                            child: Text(
                              'Grand Canyon National Park',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Text(
                            'Arizona, United States',
                            style: TextStyle(color: Colors.grey),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                  thickness: .2,
                  indent: 8,
                  endIndent: 8,
                ),
                Container(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(8.0, 0, 0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'TRIP DETAILS',
                          style: TextStyle(color: Colors.grey[700]),
                        ).tr(),
                        Text(
                          '2 Days and 1 Night Camping',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ).tr(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'DATE OF TRAVEL',
                          style: TextStyle(color: Colors.grey[700]),
                        ).tr(),
                        Text(
                          '15 Nov 2020 at 8:30 AM',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ).tr(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'TOTAL AMOUNT',
                          style: TextStyle(color: Colors.grey[700]),
                        ).tr(),
                        Text(
                          '\$680.00',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Divider(
                  color: Colors.black,
                  thickness: .2,
                  indent: 8,
                  endIndent: 8,
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Booking starts from 28 Oct 2020',
                          style: TextStyle(color: Colors.grey[700]),
                        ).tr(),
                        GestureDetector(
                          onTap: null,
                          child: Container(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class FloatingIconButton extends StatelessWidget {
  final Function onPressed;
  final Color buttonColor;
  final IconData icon;
  final Color color;

  const FloatingIconButton({Key key,
    @required this.onPressed,
    this.buttonColor,
    @required this.icon,
    this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        onPressed: onPressed,
        backgroundColor: buttonColor ?? Colors.indigo,
        child: Icon(icon));
  }
}

class RoundedButtonWithIcon extends StatelessWidget {
  final double radius;
  final Color splashColor;
  final Color textColor;
  final Color buttonColor;
  final String title;
  final Function onPressed;
  final IconData icon;

  const RoundedButtonWithIcon({Key key,
    this.radius,
    this.splashColor,
    this.textColor,
    this.buttonColor,
    @required this.title,
    @required this.onPressed,
    @required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(radius ?? 50),
      onTap: onPressed,
      splashColor: splashColor ?? Colors.blue,
      child: Ink(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(radius ?? 50),
            color: buttonColor ?? Colors.blue,
            boxShadow: [BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.08))]),
        child: Padding(
            padding: const EdgeInsets.fromLTRB(12, 12, 12, 12),
            child: Row(
              children: <Widget>[

                Icon(
                  icon,
                  color: textColor ?? Colors.white,
                ),
                Text(
                  "$title",
                  style: TextStyle(color: textColor ?? Colors.white),
                ),

              ],
            )),
      ),
    );
  }
}

class PrimaryLineButton extends StatelessWidget {
  final String title;
  final Function onPressed;

  const PrimaryLineButton(
      {Key key, @required this.title, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlineButton(
      borderSide: BorderSide(color: Color(0xFF1568F2)),
      onPressed: onPressed,
      child: Text("$title"),
    );
  }
}

class GredientButton extends StatelessWidget {
  final double radius;
  final Color splashColor;
  final Color textColor;
  final Color buttonColor;
  final String title;
  final Function onPressed;
  final List<Color> colors;

  const GredientButton({Key key,
    this.radius,
    this.splashColor,
    this.textColor,
    this.buttonColor,
    @required this.title,
    @required this.onPressed, @required this.colors})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(radius ?? 6),
      onTap: onPressed,
      splashColor: splashColor ?? Colors.blue,
      child: Ink(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius ?? 6),
          gradient: LinearGradient(colors: colors),
          color: buttonColor ?? Colors.blue,

          boxShadow: [BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.08))],),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            "$title",
            style: TextStyle(color: textColor ?? Colors.white),
          ),
        ),
      ),
    );
  }
}
