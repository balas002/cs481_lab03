import 'package:flutter/material.dart';

import 'PageTwo.dart';

class DataTableInfo extends StatelessWidget {
  static const String _title = 'Yoda\'s Travel';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(_title),
          backgroundColor: Colors.indigo,
        ),
        body: Center(child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DataTableInteractive(),
            SizedBox(height: 30),
            FloatingIconButton(
              buttonColor: Colors.indigoAccent,
              icon: Icons.arrow_back,
              onPressed: () => Navigator.pop(context),
            )
          ],
        )),
      ),
    );
  }
}

class TitleMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 50,
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.indigo
      ),
      child: Center(child: Text('MENU', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.white),)),
    );
  }
}


class DataTableInteractive extends StatefulWidget {
  @override
  _DataTableInteractiveState createState() => _DataTableInteractiveState();
}

class _DataTableInteractiveState extends State<DataTableInteractive> {
  List<Map<String, String>> dataRows = [
    {"Name": "Noah", "Age": "12", "Pass": "Child", "Price": "100\$"},
    {"Name": "Blaise", "Age": "48", "Pass": "Adult", "Price": "240\$"},
    {"Name": "Lucas", "Age": "22", "Pass": "Student", "Price": "170\$"},
    {"Name": "Marie", "Age": "21", "Pass": "Student", "Price": "170\$"}
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      width: 340,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.indigo, Colors.indigo.withOpacity(0.8), Colors.indigoAccent.withOpacity(0.8), Colors.indigoAccent]),
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
              color: Colors.indigo,
              spreadRadius: 2,
              blurRadius: 4,
              offset: Offset(0, 3)),
        ],
      ),
      child: DataTable(
        //dividerThickness: 0,
        columnSpacing: 10.0,
        horizontalMargin: 30,
        columns: const <DataColumn>[
          DataColumn(
            label: Text(
              'Name',
              style: TextStyle(
                  color: Colors.white, fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              'Age',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              'Pass',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              'Price',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.italic),
            ),
          ),
        ],
        rows: dataRows
            .map(
          ((elem) => DataRow(cells: <DataCell>[
            DataCell(Text(elem["Name"],
                style:
                TextStyle(color: Colors.white, fontSize: 18))),
            DataCell(Text(elem["Age"],
                style:
                TextStyle(color: Colors.white, fontSize: 18))),
            DataCell(Text(elem["Pass"],
                style:
                TextStyle(color: Colors.white, fontSize: 18))),
            DataCell(Text(elem["Price"],
                style:
                TextStyle(color: Colors.white, fontSize: 18)))
          ])),
        )
            .toList(),
      ),
    );
  }
}
